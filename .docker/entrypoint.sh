#!/bin/sh

usermod -ou `stat -c %u /workspace` apache || true
groupmod -og `stat -c %g /workspace` apache || true

if [ "$1" = "/sbin/runit-wrapper" ]; then
    exec "$@"
else
    su apache -s /bin/sh -c "$*"
fi
