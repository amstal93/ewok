EXEC := docker-compose exec -u apache $(DOCKER_EXEC_FLAGS) httpd

ifeq ($(shell uname -s),Darwin)
	DOCKER_COMPOSE_OVERRIDE_FILE := .docker/docker-compose.mac.yml
else
	DOCKER_COMPOSE_OVERRIDE_FILE := .docker/docker-compose.unix.yml
endif

# Environment Control ----------------------------------------------------------

.PHONY: install clean help

install: up vendor/ public/build/ data-schema data-fixtures

clean: down
	rm -rf node_modules/ public/build/ var/ vendor/

help:

# Docker Tools -----------------------------------------------------------------

.PHONY: up down sh

up: docker-compose.override.yml
	docker-compose up -d --remove-orphans

down:
	docker-compose down

sh:
	$(EXEC) /bin/sh

docker-compose.override.yml: $(DOCKER_COMPOSE_OVERRIDE_FILE)
	cp $(DOCKER_COMPOSE_OVERRIDE_FILE) docker-compose.override.yml

# Application Tools ------------------------------------------------------------

.PHONY: data-schema data-fixtures data-diff

vendor/: composer.lock
	$(EXEC) composer install --no-scripts --no-suggest

node_modules/: package.json yarn.lock
	$(EXEC) yarn install

public/build/: webpack.config.js node_modules/ $(shell find assets -type f  | sed 's/ /\\ /g')
	$(EXEC) yarn encore dev

data-schema:
	$(EXEC) bin/console doctrine:database:create --if-not-exists -n
	$(EXEC) bin/console doctrine:migrations:migrate --allow-no-migration -n

data-fixtures:
	$(EXEC) bin/console doctrine:fixtures:load -n

data-diff:
	$(EXEC) bin/console doctrine:cache:clear-metadata -n
	$(EXEC) bin/console doctrine:migrations:diff -n

# Quality Assurance ------------------------------------------------------------

.PHONY: lint-twig lint-yaml phpcs phpcs-fix phpunit

lint-twig:
	$(EXEC) bin/console lint:twig templates/

lint-yaml:
	$(EXEC) bin/console lint:yaml config/ translations/ *.yml

phpcs:
	$(EXEC) vendor/bin/php-cs-fixer fix --diff --dry-run --no-interaction -v

phpcs-fix:
	$(EXEC) vendor/bin/php-cs-fixer fix -vvv

phpunit:
	$(EXEC) bin/console doctrine:database:drop --if-exists --force --env=test
	$(EXEC) bin/console doctrine:database:create -n --env=test
	$(EXEC) bin/console doctrine:schema:create --env=test
	$(EXEC) bin/console doctrine:fixtures:load -n --env=test
	$(EXEC) bin/phpunit
